#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

int main(int argc, char **argv) {
	Display* display = XOpenDisplay(0);

	if (!display) {
		printf("Are you not in an X context? This program doesn't make sense outside an X context.\n");
		return 2;
	}

	Window root = DefaultRootWindow(display);
	XEvent ev;
	XKeyEvent* kev;

	if (argc < 2) {
		printf("Usage:\n");
		printf("  waitforkey 123\n");
		printf("\n");
		printf("Wait for key with scan code 123 to be pressed.  Use xev to get scan codes\n\n");
		return 1;
	}

	int keycode = atoi(argv[1]);
	printf("Waiting for %d\n", keycode);

	unsigned int modifiers = AnyModifier;

	XGrabKey(display, keycode, modifiers, root, False, GrabModeAsync, GrabModeAsync);

	XSelectInput(display, root, KeyPressMask);
	while (1) {
		int shouldQuit = 0;
		XNextEvent(display, &ev);
		switch (ev.type) {
			case KeyPress:
				kev = (XKeyEvent*)&ev;
				XUngrabKey(display, keycode, modifiers, root);
				shouldQuit = 1;
				break;
			default:
				break;
		}
		if (shouldQuit)
			break;
	}

	XCloseDisplay(display);
	return 0;
}
